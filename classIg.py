import requests as req
import time, hashlib, uuid, hmac, urllib, json

class instagramApi:
    def getCSRF(self):
        getcsrf = req.get('https://www.instagram.com').text
        firstSplit = getcsrf.split('"csrf_token":"')[1]
        return firstSplit.split('"')[0]

    def generateDeviceId(self):
        timestring = str(time.time())
        ngeMD5 = hashlib.md5(timestring.encode())
        return 'android-' + ngeMD5.hexdigest()[:16]

    def generateUUID(self):
        return str(uuid.uuid4())

    def hookGenerate(self, hookString):
        createEncrypt = hmac.new(b'5bd86df31dc496a3a9fddb751515cc7602bdad357d085ac3c5531e18384068b4', hookString.encode(), hashlib.sha256)
        return 'ig_sig_key_version=4&signed_body='+createEncrypt.hexdigest()+'.'+urllib.parse.quote_plus(hookString)

    def getCookies(self, cookie_jar, domain):
        cookie_dict = cookie_jar.get_dict(domain=domain)
        found = ['%s=%s' % (name, value) for (name, value) in cookie_dict.items()]
        return ';'.join(found)
